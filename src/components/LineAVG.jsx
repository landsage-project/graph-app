import React, { useState, useCallback } from "react";
import "../App.css";

import Form from "react-bootstrap/Form";



export default function LineAVG({ state}) {
    // const [stateLine, setStateLine] = useState(state);
    const [isShown, setIsShown] = useState(state.isShownAvg);
    
    // const stateLine = state;
    // const setStateLine = setState;

    // console.log(isShown, state.isSync);
    // setStateLine((prevState, props) => { return { isSync: !prevState.isSync }});

    // console.log(isShown, state.isSync);

  // setState({ isShownAvg: isShown });
    const toggleIsShownAvgLine = useCallback(() => {
        //console.log(`>>Handle Toggle Showing Sponser`);
        
        setIsShown((prevState, props)=>{return !isShown});
        state.isShownAvg = isShown;
        
        // setStateLine({isShownAvg: !isShown});
        // this.setState((prevState, props) => { return { isShownAvg: isShown }});

      }, [state, isShown]);


      return (
        <span>
          <Form className="d-inline">
            <Form.Check
              checked={isShown}
              inline
              type="switch"
              id="custom-switch-average-line"
              label="Average Line"
              //checked
              onChange={toggleIsShownAvgLine}
            />
          </Form>
        </span>
      );

}