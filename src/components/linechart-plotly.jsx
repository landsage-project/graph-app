import React, { Component } from "react";
import Plot from "react-plotly.js";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
const randomColor = require("randomcolor");
const metaDataColor = {
  "Rainfall":{
    NoRain:'#8a8a8a',
    LightRain:'#3974cc',
    ModerateRain:'#e3b132',
    HeavyRain:'#c9632c',
    VeryHeavyRain:'#9e1006',
  },
  "WaterLevel":{
    AlarmLevel:'#e3b132',
    FloodLevel:'#9e1006'
  }
}
class LineChartPlotly extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: this.props.dataSource,
      dataPropertyName: this.props.dataPropertyName,
      handleSetRange: this.props.handleSetRange,
      startDateRange: this.getLastMonthDate(),
      endDateRange: this.getTodayDate(),
      handleSocketChangeDateRange: this.props.handleSocketChangeDateRange,
      handleSocketChangeDateXRangeSlider:this.props.handleSocketChangeDateXRangeSlider,
      handleToggleIsFillFrame: this.props.handleToggleIsFillFrame,
      handleRandomNewColor: this.props.handleRandomNewColor,
      fixColor: "#11406b ",
      metaData: this.props.metaData,
      data: [],
      layout: {},
      frames: [],
      config: {},
      isShownAvg: this.props.isShownAvg,
      typeName: this.props.typeName,
      forecastData: this.props.forecastData,
    };
    //console.log(`FillFrame Option: ${this.props.isFillFrame}`);
  }

  componentDidUpdate(prevProps) {
    if (this.props.isShownAvg !== prevProps.isShownAvg) {
      // console.log("refresh Line chart plotly if isShownAvg changed!", )
      const newData = this.processData();
      this.setState({ data: newData });
    }

    // RangeSlider side effect
    if (this.props.xAxisRange !== prevProps.xAxisRange) {
      // console.log("xAxisRange has been changed");
      // console.log(this.props.xAxisRange);
      const newLayout = this.processLayout();
      this.setState({ layout: newLayout });
    }

    // Fill Frame side effect
    if(this.props.isFillFrame !== prevProps.isFillFrame) {
      // console.log("isFillFrame has been changed");
      // console.log(this.props.isFillFrame);
      const newConfig = this.processConfig();
      this.setState({ config: newConfig });
    }

    // Random Color side effect
    //console.log(`isCustomColor: ${this.props.isCustomColor}`);
    if(this.props.isCustomColor !== prevProps.isCustomColor){
      // console.log("isCustomColor has been changed");
      // console.log(this.props.isCustomColor);
      const newData = this.processData();
      this.setState({ data: newData });
    }else if (this.props.customColor !== prevProps.customColor)
    {
      // console.log("customColor has been changed");
      // console.log(this.props.customColor);
      const newData = this.processData();
      this.setState({ data: newData });
    }

    // New DataJson side effect
    if(this.props.dataJSONObj !== prevProps.dataJSONObj)
    {
      // console.log("dataJSONObj has been changed");
      // console.log(this.props.dataJSONObj);
      const newData = this.processData();
      this.setState({ data: newData });
    }
  }

  handleRefreshServerData = async () => {
    // console.log("Handle refresh server data - it might take time");
    this.props.handleRefreshServerData();
  };

  handleDateRangeInputChange = (event) => {
    // console.log("Hanle date change range");
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({ [name]: value });
    // console.log("Handle Input Change: " + name + ":" + value);
  };

  handleDateRangeInputSubmit = (event) => {
    // console.log(
    //   "Handle Input Date Submit: " +
    //     this.state.startDateRange +
    //     ":" +
    //     this.state.endDateRange
    // );
    this.state.handleSetRange([
      this.state.startDateRange,
      this.state.endDateRange,
    ]);

    event.preventDefault();
  };

  getLastMonthDate = () => {
    const today = new Date();
    const d = today.getDate();
    const m = today.getMonth();
    const y = today.getFullYear();
    const lastMonth = new Date(y, m - 1, d);
    const lastMonthString = lastMonth.toISOString().split("T")[0];
    //console.log(lastMonthString);
    return lastMonthString;
  };

  getTodayDate = () => {
    const today = new Date();
    const todayString = today.toISOString().split("T")[0];
    //console.log(todayString);
    return todayString;
  };

  handleRelyOutEvent = (eventData) => {
    // console.log("Plotly: You RELAYED OUT!!!!!");
    // console.log(eventData);
    // Auto Range triggered when reset axis
    if(eventData["xaxis.autorange"] === true)
    {
      // console.log("xaxis.autorange - true : Reset range");
      // Get Current min-max range
      //console.log(this.state.data);
      const data = this.state.data;
      const xLabels = data[0].x;
      //console.log(xLabels.length);
      if(xLabels && xLabels.length >= 2)
      {
        const xRangeData = {
          xrange: [xLabels[0], xLabels[xLabels.length - 1]],
        };
        // Socket operation - receive a date to be emit
        // Not update front-end on this behaviour, it is not suitable to use
        // middle date of all-data range for date selection, kind of reset range instead
        // this.state.handleSocketChangeDateRange(
        //   //result.toISOString().split("T")[0]
        //   xLabels[(xLabels.length - 1)/2]
        // );
        // Limitation: only the max range of this data is broadcast
        this.state.handleSocketChangeDateXRangeSlider(xRangeData);
      }
      

    }else
    // Pan Mode
    if (eventData["xaxis.range[0]"] && eventData["xaxis.range[1]"]) {
      // console.log("Pan Data---");
      // console.log(`slider range[0] = ${eventData["xaxis.range[0]"]}`);
      // console.log(`slider range[1] = ${eventData["xaxis.range[1]"]}`);
      const d1msecs = new Date(eventData["xaxis.range[0]"]).getTime(); // get milliseconds
      const d2msecs = new Date(eventData["xaxis.range[1]"]).getTime(); // get milliseconds

      const avgTime = (d1msecs + d2msecs) / 2;
      //console.log(avgTime);

      const result = new Date(avgTime);
      // console.log(`date in middle is ${result.toISOString().split("T")[0]}`);
      // Socket operation - receive a date to be emit
      this.state.handleSocketChangeDateRange(
        result.toISOString().split("T")[0]
      );
      this.state.handleSocketChangeDateXRangeSlider({
        xrange: [eventData["xaxis.range[0]"], eventData["xaxis.range[1]"]],
      });
    } // Select only one behaviour
    else if (eventData["xaxis.range"]) {
      // Range Slider
      // console.log("SliderRange Data---");
      // console.log(`slider range[0] = ${eventData["xaxis.range"][0]}`);
      // console.log(`slider range[1] = ${eventData["xaxis.range"][1]}`);

      const d1msecs = new Date(eventData["xaxis.range"][0]).getTime(); // get milliseconds
      const d2msecs = new Date(eventData["xaxis.range"][1]).getTime(); // get milliseconds

      const avgTime = (d1msecs + d2msecs) / 2;
      //console.log(avgTime);

      const result = new Date(avgTime);
      // console.log(`date in middle is ${result}`);
      // Socket operation - receive a date to be emit
      this.state.handleSocketChangeDateRange(
        result.toISOString().split("T")[0]
      );
      this.state.handleSocketChangeDateXRangeSlider({
        xrange: [eventData["xaxis.range"][0], eventData["xaxis.range"][1]],
      });

      //ISSUE:It might fire too many event, emit only on mouse released is prefered
      //https://community.plotly.com/t/rangeslider-triggered-and-get-range/14540
    }
  };
  // console.log(this.state.isShownAvg);
  processData = () => {
    // console.log(this.state.isShownAvg);
    const {metaData} = this.state;
    console.log('metaData',metaData);
    const labelIndexes = this.props.dataJson.data.map((i) => i.date);
    const valueIndexes = this.props.dataJson.data.map((i) => i.value);
    console.log('labelIndexes',labelIndexes);
    console.log('valueIndexes',valueIndexes);

    let labelIndexesTimeSeriesPredict = [];
    let valueIndexesPredict = [];
    if(this.state.typeName === "WaterLevel"){
      let dayForPredict = 7;
      let lastDay = new Date(labelIndexes[labelIndexes.length - 1]);
      labelIndexesTimeSeriesPredict.push(lastDay.getFullYear() + '-' + (lastDay.getMonth()+1)+ '-' + lastDay.getDate());
      valueIndexesPredict.push(valueIndexes[valueIndexes.length - 1]);
      valueIndexesPredict = valueIndexesPredict.concat(this.state.forecastData);
      console.log('this.state.forecastData',this.state.forecastData);
      console.log('valueIndexesPredict',valueIndexesPredict);

      for(let day = 1; day <= dayForPredict; day++){
        let date = new Date(lastDay.setDate(lastDay.getDate() + 1));
        labelIndexesTimeSeriesPredict.push(date.getFullYear() + '-' + (date.getMonth()+1)+ '-' + date.getDate());
      }
    }
    console.log('valueIndexesPredict',valueIndexesPredict);
    console.log('labelIndexesTimeSeriesPredict',labelIndexesTimeSeriesPredict);


    const data = [
      {
        x: labelIndexes,
        y: valueIndexes,
        type: "scatter",
        //type: "bar",
        // mode: "lines+markers",
        mode: "lines",
        marker: {
          //color: newRandomMarkerColor,
          color: this.state.fixColor,
          line: {
            width: 1,
          },
        },
        //line: { color: newRandomMarkerColor },
        line: {
          color: this.props.isCustomColor
            ? this.props.customColor
            : this.state.fixColor,
        },
        name: this.props.dataPropertyName,
        //hovertext: "Text under data on hover",
        //hoverinfo: "x+y",
      }

    ];
    
    if(this.state.typeName === "WaterLevel"){
      data.push({
        x: labelIndexesTimeSeriesPredict,
        y: valueIndexesPredict,
        type: "scatter",
        mode: "lines",
        marker: {
          color: this.state.fixColor,
          line: {
            width: 1,
          },
        },
        line: {
          color: "#FF4500",
        },
        name: "Forecast WaterLevel",
      })
    }
    
    if(this.props.isShownAvg){
      const labelIndexAvg = labelIndexes.concat(labelIndexesTimeSeriesPredict);
      metaData.forEach(item => {
        const {Key, Value} = item;
        console.log(`${Key} : ${Value}`);
        const values = valueIndexes.map(item => Value)
        const valuesX = values.concat(labelIndexesTimeSeriesPredict.map(item => Value));
        data.push(
          {
            x: labelIndexAvg,
            y: valuesX,
            mode: 'lines',
            name: Key,
            line: {
              color: metaDataColor[this.props.typeName][Key],
              width: 3
            }
          }
        )  



      })
          
    }



    return data;
  };

  processLayout = () => {
    const layout = {
      dragmode: "pan", // set default operation as a pan
      useResizeHandler: true,
      datarevision: this.props.uiRevisionNo,
      //width: 1200,
      //height: 600,
      title: this.props.dataPropertyName,
      //showlegend: true,
      font: { size: 14 },
      automargin: true,
      autosize: true, //Automatic resize to fit the screen
      //hovermode: "x unified",
      hovermode: "x unified",
      //hovermode: "closest",
      margin: {
        l: 80,
        r: 100,
        b: 200,
        t: 95,
        pad: 0,
      },
      //dragmode: "pan", // make default interaction as pan
      // Fix value such as water level to not scroll across range
      yaxis: {
        //autorange: "reversed",
        fixedrange: true,
        rangemode: "tozero", // Want it to scale from zero or not
        title: "Value",
        zeroline: true,
        zerolinecolor: "LightGrey",
        ticks: "outside",
        tickcolor: "crimson",
        ticklen: 10,
      },
      xaxis: {
        //autorange: true,
        autorange: !this.props.isSetRange, // If the range has been set by drawchartfromurl, disable it
        range: this.props.xAxisRange, // Receive range spec from the drawchartfromurl
        //range: ["14-Jan-1923", "28-Jan-1923"],
        //range: ["2016-07-01", "2016-12-31"],
        //range: [],
        ticks: "inside",
        tickcolor: "crimson",
        ticklen: 5,
        //showticklabels: false,
        //tickprefix: "Y",
        showgrid: true,

        type: "date",
        fixedrange: false, // Allow/not the slide to go further than given range
        title: "Date",
        rangeslider: false, // slider at the end of graph

        // Create a range selector
        rangeselector: {
          buttons: [
            {
              step: "month",
              stepmode: "backward",
              count: 1,
              label: "1m",
            },
            {
              step: "month",
              stepmode: "backward",
              count: 3,
              label: "3m",
            },
            {
              step: "month",
              stepmode: "backward",
              count: 6,
              label: "6m",
            },
            {
              step: "year",
              stepmode: "todate",
              count: 1,
              label: "YTD",
            },
            {
              step: "year",
              stepmode: "backward",
              count: 1,
              label: "1y",
            },
            {
              step: "year",
              stepmode: "backward",
              count: 5,
              label: "5y",
            },
            {
              step: "year",
              stepmode: "backward",
              count: 10,
              label: "10y",
            },
            {
              step: "all",
            },
          ],
        },
      },
    };
    return layout;
  };

  processConfig = () => {
    const config = {
      autosizable: true,
      scrollZoom: true,
      editable: false,
      staticPlot: false, // make static chart,
      displayModeBar: true, // Make tool on top alway available
      // toImageButtonOptions: {
      //   format: "svg", // one of png, svg, jpeg, webp
      //   filename: "custom_image",
      //   height: 500,
      //   width: 700,
      //   scale: 1, // Multiply title/legend/axis/canvas sizes by this factor
      // },
      displaylogo: false,
      responsive: true,
      doubleClickDelay: 400, // the maximum delay between two consecutive clicks to be interpreted as a double-click in m
      modeBarButtonsToRemove: [
        //"zoom2d",
        //"pan2d",
        //"select2d",
        //"lasso2d",
        //"zoomOut2d",
        //"zoomIn2d",
      ],
      fillFrame: this.props.isFillFrame ? true : false, // Make the chart fully fit the page container
      frameMargins: 0,
      showAxisDragHandles: true, // enable or disable dragging handle on axis
    };
    return config;
  }

  componentDidMount() {
    // console.log("Mounting Graph Plot");
    // console.log("Pre-process data");
    const data = this.processData();
    // console.log("Setup Graph props: Data");
    // console.log("Setup Graph props: Layout");
    const layout = this.processLayout();

    // console.log("Setup Graph props: Config");
    const config = this.processConfig();
    // console.log("Updating Graph State");
    this.setState({ data: data, layout: layout, config: config });
  }

  render() {
    console.log("refresh Line chart plotly render")
    return (
      <div className="graph-container">
        <form onSubmit={this.handleDateRangeInputSubmit}>
          <a className="link-secondary" href={this.props.dataSource}>
            Data Source
          </a>{" "}
          <input
            type="date"
            name="startDateRange"
            value={this.state.startDateRange}
            onChange={this.handleDateRangeInputChange}
            min="1500-1-1"
            max="9999-12-31"
            pattern="\d{4}-\d{2}-\d{2}"
            required
          />{" "}
          to{" "}
          <input
            type="date"
            name="endDateRange"
            value={this.state.endDateRange}
            onChange={this.handleDateRangeInputChange}
            min="1500-1-1"
            max="9999-12-31"
            pattern="\d{4}-\d{2}-\d{2}"
            required
          />{" "}
          <Button
            variant="secondary"
            size="sm"
            type="submit"
            value="UpdateRange"
          >
            SetRange
          </Button>{" "}
          <Button
            variant="secondary"
            size="sm"
            onClick={this.handleRefreshServerData}
            style={{ display: "inline" }}
          >
            Reload Data
          </Button>
        </form>
        <Form className="d-inline">
          <Form.Check
            checked={this.props.isFillFrame}
            inline
            type="switch"
            id="custom-switch-fill-frame"
            label="Fill Frame"
            //checked
            onChange={this.state.handleToggleIsFillFrame}
          />
        </Form>
        <Button
          variant="outline-primary"
          size="sm"
          onClick={this.state.handleRandomNewColor}
          style={{ display: "inline" }}
        >
          Random Color
        </Button>

        <Plot
          //onClick={this.handlePlotClickEvent}
          //onRelayouting={this.handleRelayOutingEvent}
          onRelayout={this.handleRelyOutEvent}
          data={this.state.data}
          layout={this.state.layout}
          config={this.state.config}
          onInitialized={(figure) => this.setState(figure)}
          onUpdate={(figure) => this.setState(figure)}
          style={{ width: "100%", height: "100%" }}
        />
      </div>
    );
  }
}

export default LineChartPlotly;
