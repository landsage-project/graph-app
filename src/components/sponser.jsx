import React, { useState, useCallback } from "react";
import "../App.css";
import LogoASIImage from "../logo/logo_Asi_at_Connect_logo-PNG.png";
import LogoEUImage from "../logo/logo_EU_emblem-PNG.png";
import LogoLandSAGEImage from "../logo/logo_LandSage-01.png";
import LogoTeinImage from "../logo/logo_TEIN-03.png";

import Image from "react-bootstrap/Image";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Form from "react-bootstrap/Form";



export default function Sponser({ isShownSponser}) {
    const [isShown, setIsShown] = useState(isShownSponser);

    const toggleIsShownSponser = useCallback(() => {
        //console.log(`>>Handle Toggle Showing Sponser`);
        setIsShown((prevState, props)=>{return !isShown});
      }, [isShown]);

    const verticalStyle = {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    };

    if(isShown)
    {
        return (
          <span>
            <Form className="d-inline">
              <Form.Check
                checked={isShown}
                inline
                type="switch"
                id="custom-switch-sponser"
                label="Credits"
                //checked
                onChange={toggleIsShownSponser}
              />
            </Form>
            <Container fluid className="bg-light">
              <Row>
                <Col xs={2} s={1} md={1} l={1} xl={1} style={verticalStyle}>
                  <Image
                    src={LogoLandSAGEImage}
                    className="img-fluid"
                    alt="LandSAGE3"
                    title="LandSAGE3"
                  />
                </Col>
                <Col xs={2} s={1} md={1} l={1} xl={1} style={verticalStyle}>
                  <Image
                    src={LogoASIImage}
                    className="img-fluid"
                    alt="ASI@Connect"
                    title="ASI@Connect"
                  />
                </Col>
                <Col xs={2} s={1} md={1} l={1} xl={1} style={verticalStyle}>
                  <Image
                    src={LogoTeinImage}
                    className="img-fluid"
                    alt="TEIN* Cooperation Center (TEIN*CC)"
                    title="TEIN* Cooperation Center (TEIN*CC)"
                  />
                </Col>
                <Col xs={2} s={1} md={1} l={1} xl={1} style={verticalStyle}>
                  <Image
                    src={LogoEUImage}
                    className="img-fluid"
                    alt="EU funding | European Union"
                    title="EU funding | European Union"
                  />
                </Col>
              </Row>
            </Container>
          </span>
        );
    }else{
        return (
          <Form className="d-inline">
            <Form.Check
              checked={isShown}
              inline
              type="switch"
              id="custom-switch-sponser"
              label="Credits"
              //checked
              onChange={toggleIsShownSponser}
            />
          </Form>
        );
    }
}