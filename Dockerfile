## Docker File: LandSage3 graph-app
## Maintainer: PISIT PRAIWATTANA
## Use multi-stage builds; build , production
# ***************** build stage *****************
FROM node:alpine  as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
#ARG HOST_URL=''
# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY . .
#RUN HOST_URL=$HOST_URL npm run build
RUN npm run build
# ***************** production stage *****************
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 3006
CMD ["nginx", "-g", "daemon off;" ]