# Graph-app visualizer

Part of LandSAGE 3 production, this system is dedicated to read time-serie data from backend and display it on the plot chart.

Chart provide synchronization mechanism over LandSAGE3 socket server for panning and zooming graph over 1 Global and 4-countrie profiles {TH, VN, MM, LA}

# API

Host:Port/DataAttribute/FilenameWithExtension
 - Display graph with 'DataAttribute' parameter over the file requested named 'FilenameWithExtension' from our data server (pinmanager)

# Hotfix 4-hf3

Change MM to LA (Laos)
Update credit's icons to be smaller size for s and xs screen size

# Current Version 4

Optimize Draw ในกราฟ
มีการเช็ค State เวลา toggle value ด้วย prevProps
มีการตรวจ event props update ใน linegraph
มีการ buffer plotly state ไว้ใน componenet ทำให้เวลา update graph ตัวกราฟจะไม่ถูก reset
มีการเติม option เพื่อโชว์ sponser
แก้ปัญหาการกดปุ่ม all/reset graph ให้มีการส่ง xmin-xmax ไปให้ graph อื่น
ทำให้มี limitation:
    เวลา reset graphไหน , min-max range ของกราฟนั้นจะ broadcast ไปกราฟอื่น แปลว่า 
    ถ้า 
    a. มี 1-5  แล้ว b มี 2-10

    เวลา reset กราฟ a.) 
    สองมันจะโดนตั้งไปที่ 1-5
    
    Assumption:
    การ auto-scale ทั้งกราฟเป็นการรีเซ็ทจริงๆ จึงอาจจะทำให้ตัว web-app รวนได้ หาก data range ใหญ่
    ระบบจึงไม่ทำการ emit CHANGE_DATE_RANGE ไปให้ front-end หากมีการกด reset axis

# Current Version 3

version นี้มี update ดังนี้
1.) Optimize draw การ update กราฟ ที่มันกระตุกก่อนนี้ เกิดจาก เราส่ง data 2 ชนิดเข้า socket server a.) date-time สำหรับ front-end ของเอฟ b.) range สำหรับ slider ของกราฟ
=> Solution คือ กราฟ กับ front-end จะ ส่ง a.) และ b.) ออกไป แต่ front-end จะรับค่า a.) เท่านั้น ส่วนกราฟ จะรับค่า b.) เท่านั้น ไม่งั้นจะ render 2 ที

2.) ย้าย socket connection ลงมา layer กลาง
[ระบบมี 3 layer App -> Draw -> GraphRender]
communication จะเข้า layer กลาง ทำให้มีการ draw หน้าใหม่ น้อยลง , ถ้าเข้า layer APP จะ refresh ทั้งหน้าเวลาได้ communication package

3.) เพิ่มปุ่ม Fill-Frame Toggle เพื่อรองรับการเปลี่ยนขนาดหน้าจอ สามารถกดปิด แล้วเปิดใหม่ เพื่อให้กราฟ คำนวนขนาดหน้าจอให้เท่ากับ window-size

4.) ปรับ default ของกราฟ ให้ไม่ fill frame 

Limitation:
กราฟ จะมีการรวนนิดหน่อย เวลาเปิดปิด sync
 อาการ กดเปิด-ปิด-ย้าย profile จะทำให้ การเคลื่อนกราฟ หลังจากนั้น 1 ครั้ง มีค่ากราฟที่รวน เมื่อเลื่อนอีกครั้ง ทุกอย่างจะกลับมาเป็นปรกติ 
solution: ยังไม่ทราบว่าเกิดจากอะไร แต่ถ้า เอาระบบ sync ออก ทุกอย่างกลับมาปรกติ 


# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
